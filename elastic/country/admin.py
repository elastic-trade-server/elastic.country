# -*- coding: utf-8 -*-

from django.contrib import admin
from models import *

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Country package"


class CountryAdmin(admin.ModelAdmin):
    list_display = ('name', 'full_name', 'symbol', 'code',)
    list_display_links = ('name',)
    fieldsets = (
        (None, {
            'fields':  ['name', 'full_name', 'symbol', 'code']
        }),
        )
admin.site.register(Country, CountryAdmin)
