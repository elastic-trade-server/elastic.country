# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Country package"


class Country(models.Model):
    """
    Справочник "Страны мира"
    """
    class Meta:
        verbose_name = _('Country')
        verbose_name_plural = _('Countries')

    #: Краткое наименование страны
    name = models.CharField(verbose_name=_('Name'), max_length=255)

    #: Полное наименование страны
    full_name = models.CharField(verbose_name=_('Full name'), max_length=255)

    #: Символьное обозначение страны
    symbol = models.CharField(verbose_name=_('Symbol'), max_length=3)

    #: Код страны
    code = models.PositiveIntegerField(verbose_name=_('Code'), unique=True)

    def __unicode__(self):
        return self.full_name
