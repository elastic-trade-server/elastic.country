from django.conf.urls import patterns, include
from tastypie.api import Api
from api import *

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Country package"


v1_api = Api(api_name='v1')
v1_api.register(CountryResource())

urlpatterns = patterns(
    '',
    url(r'^api/', include(v1_api.urls)),
)
